<?php
/**
 * @file
 * A specific handler for JM.
 */

$plugin = array(
  'title' => t('Address form (JM add-on)'),
  'format callback' => 'addressfield_format_address_jm_generate',
  'type' => 'address',
  'weight' => -80,
);

function addressfield_format_address_jm_generate(&$format, $address, $context = array()) {
  if ($address['country'] == 'JM' && $context['mode'] == 'form') {
     unset($format['locality_block']['postal_code']);

    // Remove the prefix from the first widget of the block.
    $element_children = element_children($format['locality_block']);
    $first_child = reset($element_children);
    unset($format['locality_block'][$first_child]['#prefix']);
    
    $format['locality_block']['locality']['#title'] = t('City/Town');
    $format['locality_block']['locality']['#required'] = FALSE;
    
    $format['locality_block']['administrative_area']['#options'] = array(
      ''   => t('--'),
      'CLA' => "Clarendon",
      'HAN' => "Hanover",
      'KIN' => "Kingston",
      'MAN' => "Manchester",
      'POR' => "Portland",
      'AND' => "St. Andrew",
      'ANN' => "St. Ann",
      'CAT' => "St. Catherine",
      'ELI' => "St. Elizabeth",
      'JAM' => "St. James",
      'MAR' => "St. Mary",
      'THO' => "St. Thomas",
      'TRL' => "Trelawny",
      'WML' => "Westmoreland",
    );
    $format['locality_block']['administrative_area']['#title'] = t('Parish');
    $format['locality_block']['administrative_area']['#required'] = TRUE;
  }
}
